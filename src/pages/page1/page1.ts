import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WelcomePage} from '../welcome/welcome'; //หน้าที่จะไป
import { TravelPage } from '../travel/travel';
import { PhonePage } from '../phone/phone';

/**
 * Generated class for the Page1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
})
export class Page1Page {
  move(){
    this.navCtrl.push(WelcomePage)
  }

  move1(){
    this.navCtrl.push(TravelPage)
  }

  move3(){
    this.navCtrl.push(PhonePage)
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Page1Page');
  }

}
